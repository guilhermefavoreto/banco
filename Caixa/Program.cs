﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Caixa
{
    class Program
    {
        static void Main(string[] args)
        {
            int novoDeposito = 1;
            int novaRetirada = 1;
            Deposito depositar = new Deposito();
            Retirada retirar = new Retirada();
            while (novoDeposito == 1)
            {
                depositar.depositar();
                Console.WriteLine("Gostaria de realizar outro depósito?");
                if (Console.ReadLine() == "nao")
                {
                    Console.WriteLine("Adeus");
                    break;
                }
                else
                    if (Console.ReadLine() == "sim")
                    {
                        novoDeposito = 1;
                    }
            }

            while (novaRetirada == 1)
            {
                retirar.retirar();
                Console.WriteLine("Gostaria de realizar outra retirada?");
                if (Console.ReadLine() == "nao")
                {
                    Console.WriteLine("Adeus");
                    break;
                }
                else
                    if (Console.ReadLine() == "sim")
                    {
                        novaRetirada = 1;
                    }
            }
        }
    }
}
