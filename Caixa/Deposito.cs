﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContaCorrenteEntities;

namespace Caixa
{
    public class Deposito
    {
        ContaCorrente agencia = new ContaCorrente();
        ContaCorrente conta = new ContaCorrente();
        /*ContaCorrente saldo = new ContaCorrente();*/
        public float ValorDeposito { get; set; }
        public void depositar()
        {
            Console.WriteLine("Bem vindo ao depósito.\n");

            Console.WriteLine("Entre com a agência.");
            agencia.Agencia = int.Parse(Console.ReadLine());

            Console.WriteLine("Entre com a conta corrente.");
            conta.Conta = int.Parse(Console.ReadLine());

            Console.WriteLine("Seu depósito será em qual moeda?");
            string moedaSwitch = Console.ReadLine();
            switch (moedaSwitch)
            {
                case "Real":
                case "1":
                    Console.WriteLine("Entre com o valor do seu depósito.");
                    ValorDeposito = float.Parse(Console.ReadLine());

                    ContaCorrente.saldoReal = ContaCorrente.saldoReal + ValorDeposito;

                    Console.WriteLine("valor depositado: " + ValorDeposito);
                    Console.WriteLine("Seu saldo total é: " + ContaCorrente.saldoReal);

                    break;

                case "Dóllar":
                case "2":
                    Console.WriteLine("Entre com o valor do seu depósito.");
                    ValorDeposito = float.Parse(Console.ReadLine());

                    ContaCorrente.saldoDollar = ContaCorrente.saldoDollar + ValorDeposito;

                    Console.WriteLine("valor depositado: " + ValorDeposito);
                    Console.WriteLine("Seu saldo total é: " + ContaCorrente.saldoDollar);

                    break;

                case "Rúpia":
                case "3":
                    Console.WriteLine("Entre com o valor do seu depósito.");
                    ValorDeposito = float.Parse(Console.ReadLine());

                    ContaCorrente.saldoRupia = ContaCorrente.saldoRupia + ValorDeposito;

                    Console.WriteLine("valor depositado: " + ValorDeposito);
                    Console.WriteLine("Seu saldo total é: " + ContaCorrente.saldoRupia);

                    break;
            }

            
        }
    }
}
