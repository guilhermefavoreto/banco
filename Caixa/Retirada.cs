﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContaCorrenteEntities;

namespace Caixa
{
    public class Retirada
    {
        string moedaSwitch = Console.ReadLine();
        ContaCorrente agencia = new ContaCorrente();
        ContaCorrente conta = new ContaCorrente();
        /*ContaCorrente saldo = new ContaCorrente();*/
        public float ValorRetirada { get; set; }
        public void retirar()
        {
            Console.WriteLine("Bem vindo à retirada.\n");

            Console.WriteLine("Entre com a agência.");
            agencia.Agencia = int.Parse(Console.ReadLine());

            Console.WriteLine("Entre com a conta corrente.");
            conta.Conta = int.Parse(Console.ReadLine());

            Console.WriteLine("Sua retirada será em qual moeda?");
            switch (moedaSwitch)
            {
                case "Real":
                case "1":
                    Console.WriteLine("Entre com o valor da sua retirada.");
                    ValorRetirada = float.Parse(Console.ReadLine());

                    ContaCorrente.saldoReal = ContaCorrente.saldoReal + ValorRetirada;

                    Console.WriteLine("valor depositado: " + ValorRetirada);
                    Console.WriteLine("Seu saldo total é: " + ContaCorrente.saldoReal);

                    break;

                case "Dóllar":
                case "2":
                    Console.WriteLine("Entre com o valor da sua retirada.");
                    ValorRetirada = float.Parse(Console.ReadLine());

                    ContaCorrente.saldoDollar = ContaCorrente.saldoDollar + ValorRetirada;

                    Console.WriteLine("valor depositado: " + ValorRetirada);
                    Console.WriteLine("Seu saldo total é: " + ContaCorrente.saldoDollar);

                    break;

                case "Rúpia":
                case "3":
                     Console.WriteLine("Entre com o valor da sua retirada.");
                    ValorRetirada = float.Parse(Console.ReadLine());

                    ContaCorrente.saldoRupia = ContaCorrente.saldoRupia + ValorRetirada;

                    Console.WriteLine("valor depositado: " + ValorRetirada);
                    Console.WriteLine("Seu saldo total é: " + ContaCorrente.saldoRupia);

                    break;
            }


        }
    }
}
