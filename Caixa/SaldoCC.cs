﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContaCorrenteEntities;

namespace Caixa
{
    public class SaldoCC
    {
        ContaCorrente agencia = new ContaCorrente();
        ContaCorrente conta = new ContaCorrente();
        //ContaCorrente saldo = new ContaCorrente();
        public void checarSaldo()
        {
            Console.WriteLine("Bem vindo ao seu saldo.");
            Console.WriteLine("Digite sua agência e conta.");
            Console.WriteLine("Agência: ");
            agencia.Agencia = int.Parse(Console.ReadLine());
            Console.WriteLine("Conta: ");
            conta.Conta = int.Parse(Console.ReadLine());

            Console.WriteLine("Seu saldo é: ");
            Console.WriteLine("R$ " + ContaCorrente.saldoReal);
            Console.WriteLine("$ " + ContaCorrente.saldoDollar);
            Console.WriteLine("INR " + ContaCorrente.saldoRupia);
        }
    }
}
