﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContaCorrenteEntities
{
    public class ContaCorrente
    {
        public static float saldoReal;
        public static float saldoDollar;
        public static float saldoRupia;
        public int Agencia { get; set; }
        public int Conta { get; set; }
    }
}
